package krishna.iskon.hkm.folkapp01;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import krishna.iskon.hkm.folkapp01.firebasedatabase.Sadhana;

/**
 * Created by Ajay on 4/23/2017.
 *
 *     {
 {
 "hostel": "madhapur",
 "name": "ajay",
 "sadhana": {
 "2017-05-10": {
 "book_reading_min": 31,
 "rounds": 5
 },
 "2017-05-11": {
 "book_reading_min": 32,
 "rounds": 5
 },
 "2017-05-12": {
 "book_reading_min": 33,
 "rounds": 5
 },
 "2017-05-13": {
 "book_reading_min": 34,
 "rounds": 5
 },
 "2017-05-14": {
 "book_reading_min": 30,
 "rounds": 5
 }
 }
 }

 *
 */


class user_singleton {
    private static final user_singleton ourInstance = new user_singleton();
    public ArrayList<String> song_names = new ArrayList<String>();
    public String user_json_url;

    public String getUser_json() {
        return user_json;
    }

    public void setUser_json(String user_json) {
        this.user_json = user_json;
    }

    public String user_json = "";
    public String getUser_email() {
        return user_email;
    }

    public void setUser_email(String user_email) {
        /*user_json_name = email (replace . with _)*/
        this.user_email = user_email;
        String tmp = user_email.replace("@gmail.com","");
        tmp = tmp.replace(".","_");
        this.user_json = tmp;
        this.user_json_url ="https://folkapp01.firebaseio.com/users/" +this.user_json+".json";
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {

        this.user_name = user_name;
    }

    public String user_email = "";
    public String user_name = "";
    public JSONObject album_single = new JSONObject();
    public ArrayList<sadhana_item> sadhana_list = new ArrayList<>();

    public JSONObject getSadhana() {
        return sadhana;
    }

    public void setSadhana(JSONObject sadhana) {
        this.sadhana = sadhana;
    }

    public JSONObject sadhana = new JSONObject();
    private static String bucket = "https://storage.googleapis.com/healyourself01/Healing%20Finalized%20Audios/";


    public void setSelected_language(String selected_language) {
        this.selected_language = selected_language;
    }

    private String selected_language = "Telugu";
    public String getAlbumid_selected() {
        return albumid_selected;
    }

    public void setAlbumid_selected(String albumid_selected) {
        this.albumid_selected = albumid_selected;
    }

    public String getSongid_selected() {
        return songid_selected;
    }

    public void setSongid_selected(String songid_selected) {
        this.songid_selected = songid_selected;
    }

    public String albumid_selected = "";
    public String songid_selected = "";

    public JSONArray getAlbums() {
        return albums;
    }

    public void setAlbums(JSONArray albums) {
        this.albums = albums;
    }

    public JSONArray albums;
    static user_singleton getInstance() {
        return ourInstance;
    }

    private user_singleton() {
    }

    /**
     *
     * @return sadhana_list
     */
    public ArrayList<sadhana_item> get_sadhana_details() {
    try {

        Iterator<String> sadhana_keys = sadhana.keys();
        sadhana_list = new ArrayList<>();
        while (sadhana_keys.hasNext()) {
            String date_item = sadhana_keys.next().toString();
            JSONObject tmp = sadhana.getJSONObject(date_item);

            int rounds = tmp.getInt("rounds");
            int book_reading_min = tmp.getInt("book_reading_min");
            sadhana_item item = new sadhana_item(date_item, rounds, book_reading_min);
            sadhana_list.add(
                    item
            );

        }
    } catch (Exception e) {
        e.printStackTrace();
    }
    return sadhana_list;
}

    /******get_sadhana_item****
     *
     * @param item_date
     * @return
     *************************/
    public sadhana_item get_sadhana_item(String item_date) {
    sadhana_item item = new sadhana_item("",0,0);
    try {
        if (sadhana.has(item_date)) {
            JSONObject tmp = sadhana.getJSONObject(item_date);
            int rounds = tmp.getInt("rounds");
            int book_reading_min = tmp.getInt("book_reading_min");
            item = new sadhana_item(item_date, rounds, book_reading_min);
        }
        else {
            item = new sadhana_item("",0,0);
        }
    }
    catch (Exception e) {
        e.printStackTrace();

    }
    return item;
}

public void set_sadhana_item(String date_key, Sadhana item) {
    try {
        JSONObject tmp = new JSONObject();
        tmp.put("rounds",item.getRounds());
        tmp.put("book_reading_min",item.getBook_reading_min());
        sadhana.put(date_key,tmp);
    } catch (Exception e) {
        e.printStackTrace();
    }
}

/*---------------------------OLD METHODS---------------------------------------*/

/*This method returns a single JSON object from the albums JSON array list*/
    public JSONObject get_single_album(int index){

        try{
            JSONObject album_single = new JSONObject();/*Initialize with null to clear old data*/
            album_single = albums.getJSONObject(index);
        }
        catch (Exception e){
            e.printStackTrace();
        }
        return album_single;
    }
    public  String getSelected_language(){
        return "";
    }
/*This method returns a single JSON object from the albums JSON array list*/

    /*********************************************************
     * get_song_names
     * Returns the list of songs with index and the language
     * @param index, Language
     * @return song_names
     *********************************************************/
    public ArrayList<String> get_song_names(int index,String Language){
                                /*Loop through the JSON Array*/
        try {
            song_names.clear();/*Initialize with null to clear old data*/
            JSONObject object0 = get_JSONObj_selected();
            ArrayList<String> songs_list = new ArrayList<String>();
            JSONArray album_songs = object0.getJSONArray(Language + "_songs");
            for(int i=0;i<album_songs.length();i++)
            {
                JSONObject object1 = album_songs.getJSONObject(i);
                String song_name = object1.getString("song_name");
                /*Check the status of song*/
                if (object1.getString("song_status").equals("true"))
                {
                    song_names.add(song_name);
                }
            }

        }
        catch (Exception e){
            e.printStackTrace();
        }
        return song_names;
    }
    /*This method returns the "album_songs" for the selected album*/
    public JSONArray get_album_songs(int index){
        try{
//             sadhana = new JSONArray();/*Initialize with null to clear old data*/
//            sadhana = get_JSONObj_selected().getJSONArray( selected_language + "_songs");
        }
        catch (Exception e){
            e.printStackTrace();
        }
        JSONArray tmp = new JSONArray();
        return tmp;
    }
    /*Returns the album_folder for the selected album*/
    public String get_album_folder (){
        String folder = "";
        try {
         folder = get_JSONObj_selected().getString("album_name");
        }
        catch (Exception e){
            e.printStackTrace();
        }
        return folder;
    }

    /*Returns the selected song name for the selected song id*/

    /*************************************************************
     * String get_selected_song_name
     * @return Selected Song
     *************************************************************/
    public String get_selected_song_name (){
        String selected_song_name = "";
        try {
//            JSONArray obj4 = get_JSONObj_selected().getJSONArray(selected_language + "_songs");
//
//            for(int i =0; i<obj4.length();i++)
//            {
//                JSONObject object4 = sadhana.getJSONObject(i);
//                String song_id = object4.getString("song_id");
//                if (song_id == songid_selected){
//                    selected_song_name = object4.getString("song_name");
//                }
//            }

        }
        catch (Exception e){
            e.printStackTrace();
        }
        return selected_song_name;
    }

    /*****************************************************************
     * get_album_cover
     * returns the album cover for the selected album.
     * @return album_cover
     *****************************************************************/
    public String get_album_cover_url(){
        String album_cover = "";
        try {

            album_cover =
                    bucket+get_JSONObj_selected().getString("album_name").replace(" ","%20")
                            +"/"+
                    get_JSONObj_selected().getString("album_cover").replace(" ", "%20");

        } catch (Exception e) {
            e.printStackTrace();
        }
        return album_cover;
    }

    /*****************************************************************
     * get_song_title
     * returns the album title for the selected album.
     * @return album_title
     *****************************************************************/
    public String get_song_title(){
        String song_title = "";
        try {

            song_title = get_selected_song_obj().getString("song_title");

        } catch (Exception e) {
            e.printStackTrace();
        }
        return song_title;
    }
    /*****************************************************************
     * get_song_desc_short
     * returns the song short desc for the selected album.
     * @return song_desc_short
     *****************************************************************/
    public String get_song_desc_short(){
        String song_desc_short = "";
        try {
            song_desc_short = get_selected_song_obj().getString("song_desc_short");

        } catch (Exception e) {
            e.printStackTrace();
        }
        return song_desc_short;
    }
    /*****************************************************************
     * get_selected_song_obj
     * returns the selected song obj
     * @return selected song obj
     *****************************************************************/
    public JSONObject get_selected_song_obj(){
        JSONObject selected_song = new JSONObject();
        try {

            JSONArray tmp5 = get_JSONObj_selected().getJSONArray(selected_language+"_songs");

            for (int i=0;i<tmp5.length();i++) {
                JSONObject tmp_song = tmp5.getJSONObject(i);
                if (tmp_song.getString("song_id") == songid_selected) {
                    selected_song = tmp_song;
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return selected_song;

    }

    /*This method returns the "album_songs" for the given index*/
    public JSONObject get_JSONObj_selected(){
        JSONObject jsonobj_selected = new JSONObject();
        try{

            for(int i = 0; i<albums.length();i++)
            {
                jsonobj_selected = albums.getJSONObject(i);
                if(jsonobj_selected.getString("album_id") == albumid_selected)
                { break;}
           }
        }
        catch (Exception e){
            e.printStackTrace();
        }
        return jsonobj_selected;
    }
}

