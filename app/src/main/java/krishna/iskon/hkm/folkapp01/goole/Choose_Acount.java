package krishna.iskon.hkm.folkapp01.goole;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;

import krishna.iskon.hkm.folkapp01.ContentPage;
import krishna.iskon.hkm.folkapp01.R;


public class Choose_Acount extends BaseActivity implements GoogleApiClient.OnConnectionFailedListener {
    private  final String TAG=Choose_Acount.this.getClass().getSimpleName();
    private static final int RC_SIGN_IN = 9001;

    //decleration of auth
    FirebaseAuth mAuth;


    private GoogleApiClient mGoogleApiClient;
    SignInButton google_signin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chooser);
        // [START config_signin]
        // Configure Google Sign In

        GoogleSignInOptions gso=new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();
        // [END config_signin]

        mGoogleApiClient=new GoogleApiClient.Builder(this)
                .enableAutoManage(this,this)
                .addApi(Auth.GOOGLE_SIGN_IN_API,gso)
                .build();
        // [START initialize_auth]
        mAuth= FirebaseAuth.getInstance();
        google_signin=(SignInButton) findViewById(R.id.sign_in_button_google);
        // [END initialize_auth]

        google_signin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//        Intent i =new Intent(MainActivity.this,GoogleActivity.class);
//                startActivity(i);

                signIn();
            }
        });

        findViewById(R.id.btn_account).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(),"it is under production...",Toast.LENGTH_LONG).show();
            }
        });
    }
    @Override
    protected void onStart() {
        super.onStart();
        FirebaseUser currentuser=mAuth.getCurrentUser();
        //updateUI(currentuser);
    }
    // [END on_start_check_user]

    private void signIn() {
        Intent signinIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signinIntent,RC_SIGN_IN);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if(requestCode==RC_SIGN_IN){
            // Google Sign In was successful, authenticate with Firebase
            GoogleSignInResult result= Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            if(result.isSuccess()){
                Toast.makeText(this,"Sign in Success.",Toast.LENGTH_SHORT).show();
                GoogleSignInAccount account=result.getSignInAccount();
                firebaseAuthWithGoogle(account);
            }
            else{
                // Google Sign In failed, update UI appropriately
                // [START_EXCLUDE]
                Toast.makeText(this,"FireBaseAuth Error:code:"+requestCode,Toast.LENGTH_SHORT).show();
                // [END_EXCLUDE]

            }

        }
    }
    private void firebaseAuthWithGoogle(GoogleSignInAccount acct){
        Log.d(TAG, "firebaseAuthWithGoogle:" + acct.getId());
        showmProgressDialog();
        AuthCredential credential= GoogleAuthProvider.getCredential(acct.getIdToken(),null);
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if(task.isSuccessful()){

                            Log.d(TAG, "signInWithCredential:success");
                            FirebaseUser user=mAuth.getCurrentUser();
                            Log.d("user email:",user.getEmail());
                            Log.d("user name:",user.getDisplayName());


                            Intent i =new Intent(Choose_Acount.this, ContentPage.class);
                            Bundle user_setdetails_bundle=new Bundle();
                            user_setdetails_bundle.putString("user_mail",user.getEmail());
                            user_setdetails_bundle.putString("user_name",user.getDisplayName());
                            user_setdetails_bundle.putInt("flag",1);
                            i.putExtras(user_setdetails_bundle);
                            startActivity(i);
                            //updateUI(user);
                        }
                        else{
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "signInWithCredential:failure", task.getException());
                            Toast.makeText(Choose_Acount.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                            //updateUI(null);
                        }
                        hideprogressDialog();


                    }
                });

    }

    private void signOut() {
        // Firebase sign out
        mAuth.signOut();
        // Google sign out
        // Google sign out
        Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                new ResultCallback<Status>() {
                    @Override
                    public void onResult(@NonNull Status status) {
                        //updateUI(null);
                    }
                });


    }
    private void revokeAccess() {
        mAuth.signOut();
        Auth.GoogleSignInApi.revokeAccess(mGoogleApiClient).setResultCallback(new ResultCallback<Status>() {
            @Override
            public void onResult(@NonNull Status status) {
                //updateUI(null);
            }
        });

    }



    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        // An unresolvable error has occurred and Google APIs (including Sign-In) will not
        // be available.
        Log.d(TAG, "onConnectionFailed:" + connectionResult);
        Toast.makeText(this, "Google Play Services error.", Toast.LENGTH_SHORT).show();

    }
}



