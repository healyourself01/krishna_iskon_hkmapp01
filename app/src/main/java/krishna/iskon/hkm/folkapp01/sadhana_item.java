package krishna.iskon.hkm.folkapp01;

/**
 * Created by Ajay on 5/14/2017.
 */

public class sadhana_item {
    public int getRounds() {
        return rounds;
    }

    public void setRounds(int rounds) {
        this.rounds = rounds;
    }

    public int getBook_reading_min() {
        return book_reading_min;
    }

    public void setBook_reading_min(int book_reading_min) {
        this.book_reading_min = book_reading_min;
    }

    public int rounds =0;
    public int book_reading_min =0;

    public String getDate_item() {
        return date_item;
    }

    public void setDate_item(String date_item) {
        this.date_item = date_item;
    }

    public String date_item= "";

    public sadhana_item(String date_item, int rounds, int book_reading_min){
        this.date_item = date_item;
        this.book_reading_min = book_reading_min;
        this.rounds = rounds;
    }
}
