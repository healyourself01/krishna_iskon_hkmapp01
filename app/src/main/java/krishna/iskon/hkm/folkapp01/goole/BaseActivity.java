package krishna.iskon.hkm.folkapp01.goole;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;

/**
 * Created by kalyani on 5/16/2017.
 */

public class BaseActivity extends AppCompatActivity {

    public ProgressDialog mProgressDialog;
    public void showmProgressDialog(){
        if(mProgressDialog == null){
            mProgressDialog=new ProgressDialog(this);
            mProgressDialog.setMessage("Loading ......");
            mProgressDialog.setIndeterminate(true);
        }
        mProgressDialog.show();
    }
    public void hideprogressDialog(){
        if(mProgressDialog!=null && mProgressDialog.isShowing()){
            mProgressDialog.dismiss();
        }
    }
    public void onStop(){
        super.onStop();
        hideprogressDialog();

    }
}
