package krishna.iskon.hkm.folkapp01;

import android.os.Bundle;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import krishna.iskon.hkm.folkapp01.firebasedatabase.Sadhana;

public class Content_SaveActivity extends AppCompatActivity {
    TextView chant_reading,book_reading;
    int chant,book;
    Button save,date_save;
    String date_save_bundle;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_content__save);
        chant_reading=(TextView)findViewById(R.id.chat_reading);
        book_reading=(TextView)findViewById(R.id.book_reading);
        date_save=(Button)findViewById(R.id.date_save);
        save=(Button)findViewById(R.id.save_date);
        Bundle b=getIntent().getExtras();
        date_save.setText(b.getString("date"));
        date_save_bundle = b.getString("date");

        /*Set the initial values from the date*/
        sadhana_item item_tmp = user_singleton.getInstance().get_sadhana_item(b.getString("date"));
        chant_reading.setText(String.valueOf(item_tmp.getRounds()));
        book_reading.setText(String.valueOf(item_tmp.getBook_reading_min()));



        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chant= Integer.parseInt(chant_reading.getText().toString());
                book=Integer.parseInt(book_reading.getText().toString());


                Sadhana sd1 = new Sadhana(chant, book);

//                String date = new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime()).toString();
                String date = date_save_bundle;
                final FirebaseDatabase database = FirebaseDatabase.getInstance();
                DatabaseReference user = database.getReference("users/" + user_singleton.getInstance().user_json);
                DatabaseReference userSadhana = user.child("sadhana");
                userSadhana.child(date).setValue(sd1);

                /*Update the JSON object in local singleton*/
                user_singleton.getInstance().set_sadhana_item(date_save_bundle, sd1);

                overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);


            }
        });

    }

}
