package krishna.iskon.hkm.folkapp01.firebasedatabase;

import com.google.firebase.database.DatabaseReference;

/**
 * Created by Ajay on 5/14/2017.
 */

public final class Sadhana {

    public int getRounds() {
        return rounds;
    }

    public void setRounds(int rounds) {
        this.rounds = rounds;
    }

    public int getBook_reading_min() {
        return book_reading_min;
    }

    public void setBook_reading_min(int book_reading_min) {
        this.book_reading_min = book_reading_min;
    }

    public int rounds =0;
    public int book_reading_min =0;

    public Sadhana(int chant_number, int book_reading_number) {
        this.rounds = chant_number;
        this.book_reading_min = book_reading_number;
    }

}
