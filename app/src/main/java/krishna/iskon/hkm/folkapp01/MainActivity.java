package krishna.iskon.hkm.folkapp01;

import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Rect;
import android.os.Bundle;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;

public class MainActivity extends AppCompatActivity {
//RECYCLER VIEW OBJECT DECLERATION
    private RecyclerView recyclerView;
    //OBJECT FOR THE ALBUMSADAPTER CLASS
    //private AlbumsAdapter adapter;
    //PASSING THE ALBUM CLASS AS A PARAMETER TO THE LIST
    private List<Album> albumList;
    JSONArray albums;
    Button btn1,btn2,btn3,btn4;
    JSONObject sadhana;
    private List<sadhana_item> sadhana_list;
    TextView user_txt_name;

    @Override
    protected void onResume() {
        super.onResume();
        user_txt_name.setText(user_singleton.getInstance().user_name);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        btn1=(Button)findViewById(R.id.btn1);
        btn2=(Button)findViewById(R.id.btn2);
        btn3=(Button)findViewById(R.id.btn3);
        btn4=(Button)findViewById(R.id.btn4);
        user_txt_name= (TextView) findViewById(R.id.user_name);
        String user_name =  user_singleton.getInstance().getUser_name();
        user_txt_name.setText(user_name);

        sadhana_list = new ArrayList<>();
        setSupportActionBar(toolbar);

       // initCollapsingToolbar();
        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i =new Intent(MainActivity.this,Content_SaveActivity.class);
                Bundle b = new Bundle();
                String date = new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime()).toString();
                b.putString("date",date);
                i.putExtras(b);
                startActivity(i);
            }
        });
        btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i =new Intent(MainActivity.this,Album_listItem.class);
                startActivity(i);
            }
        });
        btn3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i =new Intent(MainActivity.this,News_Webview.class);
                startActivity(i);
            }
        });
        btn4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i =new Intent(MainActivity.this,Events_Webview.class);
                startActivity(i);
            }
        });


        RequestQueue queue = Volley.newRequestQueue(this);



        final user_singleton user = user_singleton.getInstance();
        String url =user.user_json_url;


// R equest a string response from the provided URL.
        JsonObjectRequest jsObjRequest = new JsonObjectRequest
        (Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            sadhana = response.getJSONObject("sadhana");
                            user.setSadhana(sadhana);
                            preapreSadhanaItems();
                        }
                        catch (Exception e){
                            e.printStackTrace();
                            Log.d("VolleyError","User JSON not found.");
                        }

                     }
                            }, new Response.ErrorListener() {

                     @Override
                     public void onErrorResponse(VolleyError error) {
                    // TODO Auto-generated method stub
                         Log.d("VolleyError","User JSON not found.");

                    }        });

// Add the request to the RequestQueue.

        queue.add(jsObjRequest);

    }

    /**
     * Adding few albumitem for testing
     *
     *  "sadhana": {
     "2017-05-10": {
     "book_reading_min": 31,
     "rounds": 5
     },
     */
    private void preapreSadhanaItems() {
        /********************Load the Sadhana items into the sadhana_list********************/
        try {

            Iterator<String> sadhana_keys = sadhana.keys();
            while (sadhana_keys.hasNext()){
                String date_item = sadhana_keys.next().toString();
                JSONObject tmp = sadhana.getJSONObject(date_item);

                int rounds = tmp.getInt("rounds");
                int book_reading_min= tmp.getInt("book_reading_min");
                sadhana_item item = new sadhana_item(date_item, rounds, book_reading_min);
                sadhana_list.add(
                        item
                );

            }
            Log.d("SADHANA",sadhana_list.toString());
        }
        catch (Exception e){
            e.printStackTrace();

        }

        //adapter.notifyDataSetChanged();
    }

    /**
     * RecyclerView item decoration - give equal margin around grid item
     */
    public class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {

        private int spanCount;
        private int spacing;
        private boolean includeEdge;

        public GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge) {
            this.spanCount = spanCount;
            this.spacing = spacing;
            this.includeEdge = includeEdge;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            int position = parent.getChildAdapterPosition(view); // item position
            int column = position % spanCount; // item column

            if (includeEdge) {
                outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
                outRect.right = (column + 1) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)

                if (position < spanCount) { // top edge
                    outRect.top = spacing;
                }
                outRect.bottom = spacing; // item bottom
            } else {
                outRect.left = column * spacing / spanCount; // column * ((1f / spanCount) * spacing)
                outRect.right = spacing - (column + 1) * spacing / spanCount; // spacing - (column + 1) * ((1f /    spanCount) * spacing)
                if (position >= spanCount) {
                    outRect.top = spacing; // item top
                }
            }
        }
    }

    /**
     * Converting dp to pixel
     */
    private int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        // TODO Auto-generated method stub
        if (keyCode == KeyEvent.KEYCODE_BACK) {

            Intent i = new Intent(MainActivity.this, ContentPage.class);
            startActivity(i);
        }

        return super.onKeyDown(keyCode, event);
    }
}
