package krishna.iskon.hkm.folkapp01;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;

import krishna.iskon.hkm.folkapp01.firebasedatabase.Sadhana;

/**
 * Created by mindgame on 3/11/2017.
 */

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.RecyclerViewHolder> {
    private Context mContext;

    private ArrayList<sadhana_item> arrayList=new ArrayList<>();


    public RecyclerAdapter(Context mContext,ArrayList<sadhana_item> arrayList) {
        this.arrayList = arrayList;
        this.mContext = mContext;
    }
    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.recycle_item,parent,false);
        RecyclerViewHolder recyclerViewHolder=new RecyclerViewHolder(view);
        return recyclerViewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerViewHolder holder, final int position) {

        final sadhana_item sadhana=arrayList.get(position);
        holder.rounds_book.setText(
                String.valueOf(sadhana.getBook_reading_min()) + " min");
        holder.rounds_chant.setText( String.valueOf(sadhana.getRounds()));
        holder.date.setText( String.valueOf(sadhana.getDate_item()));
        holder.edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                /*************************change here to AlbumsAdapter.cladss*************************************/
                Intent i = new Intent(mContext.getApplicationContext(),Content_SaveActivity.class);
                Bundle b = new Bundle();
                String date_selected = arrayList.get(position).getDate_item().toString();
                b.putString("date",date_selected);
//                String songid = arrayList.get(position).getSongid();
//                user_singleton.getInstance().setSongid_selected(songid);
//                Toast.makeText(mContext,"SognId :"+songid,Toast.LENGTH_SHORT).show();
                i.putExtras(b);
                mContext. startActivity(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }
    public class RecyclerViewHolder extends RecyclerView.ViewHolder{
        TextView serial_num,date,rounds_chant,rounds_book;
        Button edit;


        Button btnitem_play;
        TextView textView1,textView2;

        public RecyclerViewHolder(View view) {
            super(view);
            serial_num=(TextView)view.findViewById(R.id.serial_number);
            date =(TextView)view.findViewById(R.id.y_d_t);
            rounds_chant=(TextView)view.findViewById(R.id.rouns_chant);
            rounds_book=(TextView)view.findViewById(R.id.round_book);
            edit=(Button) view.findViewById(R.id.edit);


        }
    }
}
