package krishna.iskon.hkm.folkapp01;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;

public class Album_listItem extends AppCompatActivity {
    RecyclerView recyclerView;
    RecyclerView.Adapter adapter;
    RecyclerView.LayoutManager layoutManager;
    ImageView album_image;
    TextView title;



    ArrayList<sadhana_item>arrayList=new ArrayList<>();

    int position;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_album_list_item);


        recyclerView= (RecyclerView) findViewById(R.id.recyclerview);
        /*albumimage*/
        album_image=(ImageView)findViewById(R.id.img_view);
        title=(TextView)findViewById(R.id.title);
        title.setText(user_singleton.getInstance().get_album_folder());
        Glide.with(getApplicationContext()).load(user_singleton.getInstance().get_album_cover_url()).into(album_image);
        load_sadhana_items();
    }

    @Override
    protected void onResume() {
        super.onResume();
        load_sadhana_items();
    }

    /*Set the songs to screen*/
public void set_songs_to_screen(){
    adapter = new RecyclerAdapter(this,arrayList );
    recyclerView.setHasFixedSize(true);
    layoutManager = new LinearLayoutManager(this);
    recyclerView.setLayoutManager(layoutManager);
    recyclerView.setAdapter(adapter);

}
public boolean onKeyDown(int keyCode, KeyEvent event) {
        // TODO Auto-generated method stub
        if (keyCode == KeyEvent.KEYCODE_BACK) {

      /*
       * Intent intent = new Intent(Intent.ACTION_MAIN);
       * intent.addCategory(Intent.CATEGORY_HOME);
       * intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
       * startActivity(intent);
       */

            Intent i = new Intent(Album_listItem.this, MainActivity.class);
            startActivity(i);
        }

        return super.onKeyDown(keyCode, event);
    }

    public void load_sadhana_items() {
                /*-----------Load the list of songs from album JSON using the index------------*/
        ArrayList<sadhana_item> sadhana_list = new ArrayList<>();
        sadhana_list = user_singleton.getInstance().get_sadhana_details();
        arrayList = sadhana_list;
/* SADHANA JSON Format
  "2017-05-10": {
        "book_reading_min": 31,
        "rounds": 5
    },
* */
        /*Todo - Need to clear the adaptor and set blank screen. data from prev session shown*/
        if (sadhana_list.iterator().hasNext()) {

            set_songs_to_screen();


        }
        else {
            Log.d("ERROR", "Album not found for index");
            Toast.makeText(getApplicationContext(), "error loading the Sadhana details", Toast.LENGTH_LONG);
        }


    }
}
