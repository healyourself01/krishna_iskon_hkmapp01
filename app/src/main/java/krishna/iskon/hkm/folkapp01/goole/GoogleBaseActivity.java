package krishna.iskon.hkm.folkapp01.goole;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.firebase.auth.FirebaseAuth;

import krishna.iskon.hkm.folkapp01.R;

/**
 * Created by kalyani on 5/17/2017.
 */

public class GoogleBaseActivity {
    private  final String TAG=GoogleBaseActivity.this.getClass().getSimpleName();
    private static final int RC_SIGN_IN = 9001;
    //decleration of auth
    FirebaseAuth mAuth;

    private GoogleApiClient mGoogleApiClient;

    private static final GoogleBaseActivity ourInstance = new GoogleBaseActivity();

    public static GoogleBaseActivity getInstance() {
        return ourInstance;
    }


}
