
1.External Storage Not working
the External storage permission has to be placed after the <Application> tag
in the manifest file.

2. Internal Storage Error
The internal storage has to be access with getFilesDir() function as given below.
getApplicationContext().getFilesDir().getPath()
https://developer.android.com/training/basics/data-storage/files.html


3. Debug Failing.
I debugged the application many times. Then debug failed.
This is not appliction error. some how the debug system gets corrupted.
I deleted the folder. Then git clone again the fresh code, clean and rebuld.
Invalidate cash and restart. It should work.

Sometimes, we have to reinstall the Android studio if it doesnt work.

4. Gradle incompatible error for cardview and recyclerview

5. The next and prev buttons were not working
I commented the following line for testing. Just uncommented it.
  mp.prepareAsync();

6. To point to the song using song id.

-----Bugs Found------------

Issue1: Play on call state idle playing even when user pauses audio
sol: use system_pause flag
Download progress bar persists
sol: destroy the download activity once download finishes.
suddenly the 2nd third screen become completely blank afte phone wakes up
the seek bar not updating in 3g in sai phone

After song finishes, the play button icon not changing
sol: use play pause call backs to change icon

The icon not changing when the play next play prev clicked.
fixed it.


Ads not loading - the ad ids should not contain any blanks. Use Trim()
The Fav failing - Use internal storage for storing the data.